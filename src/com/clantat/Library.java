package com.clantat;

public class Library {
    private Book [] library;

    public Library(Book[] library) {
        this.library = library;
    }

    public Book[] getLibrary() {
        return library;
    }
    public void setLibrary(Book[] library) {
        this.library = library;
    }
    public void addBook(Book book){
        for (int i = 0; i <library.length; i++) {
            if(library[i]==null){
                library[i] = book;
                break;
            }
        }
    }
    public void removeBook(Book book){
        for (int i = 0; i <library.length; i++) {
            if(library[i]==book){
                library[i]=null;
                break;
            }
        }
    }
    public Book searchBook(String name,String author, int year){
        for (int i = 0; i <library.length; i++) {
            if(library[i].getAuthor().equals(author)&&library[i].getName().equals(name)&&library[i].getYear()==year){
                return library[i];
            }
        }
        return null;
    }

}
