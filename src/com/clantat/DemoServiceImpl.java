package com.clantat;

public class DemoServiceImpl implements DemoService{
    @Override
    public void viewLibrary(Library foreign) {
        System.out.println();
        for (int i = 0; i <foreign.getLibrary().length; i++) {
            if(foreign.getLibrary()[i]!=null)System.out.println(foreign.getLibrary()[i].getName());
            else System.out.println(foreign.getLibrary()[i]);
        }
        System.out.println();
    }
    @Override
    public void start() {
        Book [] noname = new Book[5];
        Book master = new Book("Giorno","Andrew",1923);
        Book jojo= new Book("Cool","Pablo",1955);
        Book katara= new Book("Aang","Braian",2002);
        Book bleach= new Book("Cronical","Mitya",1340);
        Book fight= new Book("Are you kidding me?","Vladimir",2007);
        Book neon= new Book("Hello","Robin",2000);
        Library foreign = new Library(noname);
        LibraryServiceImpl foreignService = new LibraryServiceImpl();
        System.out.println("Library:");
        viewLibrary(foreign);
        foreignService.addBook(foreign,master);
        System.out.println("The first add book:");
        viewLibrary(foreign);
        foreignService.addBook(foreign,jojo);
        foreignService.addBook(foreign,katara);
        foreignService.addBook(foreign,bleach);
        foreignService.addBook(foreign,fight);
        System.out.println("The final size library:");
        viewLibrary(foreign);
        foreignService.addBook(foreign,neon);
        System.out.println("add book in Library after final size library:");
        viewLibrary(foreign);
        if(foreignService.searchBook(foreign,bleach)!=null) {
            System.out.println("Book "+bleach.getName()+" have been founded in library");
        }
        foreignService.removeBook(foreign,bleach);
        System.out.println("Remove book "+bleach.getName());
        viewLibrary(foreign);
        foreignService.addBook(foreign,neon);
        System.out.println("Add the last book:");
        viewLibrary(foreign);


    }


}
