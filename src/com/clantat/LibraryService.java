package com.clantat;

public interface LibraryService {
    void addBook(Library library, Book book);
    void removeBook(Library library,Book book);
    Book searchBook(Library library,Book book);
}
