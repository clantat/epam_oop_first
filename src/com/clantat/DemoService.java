package com.clantat;

public interface DemoService {
    void viewLibrary(Library library);
    void start();
}
