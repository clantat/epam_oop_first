package com.clantat;

public class LibraryServiceImpl implements LibraryService{

    @Override
    public void addBook(Library library, Book book) {
        library.addBook(book);
    }

    @Override
    public void removeBook(Library library, Book book) {
        library.removeBook(book);
    }

    @Override
    public Book searchBook(Library library, Book book) {
        return library.searchBook(book.getName(),book.getAuthor(),book.getYear());
    }

}
